const convertIpToNumberArray = inputString => {

    if (inputString !== null || inputString !== undefined) {

        const isValidIp = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(inputString);
        if (isValidIp) {
            const stringArray = inputString.split(".");
            const ipNumericArray = [];
            for (index = 0; index < 4; index++) {
                let ipBlock = parseInt(stringArray[index]);
                ipNumericArray.push(ipBlock);
            }
            return ipNumericArray;
        }
        return [];
    }
}

module.exports = convertIpToNumberArray;