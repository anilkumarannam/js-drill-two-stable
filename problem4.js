const titleCase = inputString => {

    if (inputString !== null && inputString !== undefined) {
        return inputString.charAt(0).toUpperCase() + inputString.slice(1).toLowerCase();
    }
    return "";
}

const fullName = person => {

    if (person !== null && person !== undefined) {
        if ('first_name' in person && 'last_name' in person && 'middle_name' in person) {
            return `${titleCase(person.first_name)} ${titleCase(person.middle_name)} ${titleCase(person.last_name)}`;
        } else if ('first_name' in person && 'last_name' in person) {
            return `${titleCase(person.first_name)} ${titleCase(person.last_name)}`;
        }
    }
    return "";
}

module.exports = fullName;