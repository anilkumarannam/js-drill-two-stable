const convertToNumericFormat = require("../problem1.js");


const number1  =  convertToNumericFormat("");                    // returns number as : 0
const number2  =  convertToNumericFormat("abcd");                // returns number as : 0
const number3  =  convertToNumericFormat("$");                   // returns number as : 0
const number4  =  convertToNumericFormat("-$");                  // returns number as : 0
const number5  =  convertToNumericFormat("--$12");               // returns number as : 0
const number6  =  convertToNumericFormat("-$$12");               // returns number as : 0
const number7  =  convertToNumericFormat("$12");                 // returns number as : 12
const number8  =  convertToNumericFormat("-$12");                // returns number as : -12
const number9  =  convertToNumericFormat("-$12.");               // returns number as : 0
const number10 =  convertToNumericFormat("-$12.1");              // returns number as : -12.1
const number11 =  convertToNumericFormat("-$12..1");             // returns number as : 0
const number12 =  convertToNumericFormat("$12.1009");            // returns number as : 12.1009
const number13 =  convertToNumericFormat("$12.1009k");           // returns number as : 0
const number14 =  convertToNumericFormat("-$3,6756.002.");       // returns number as : 0
const number15 =  convertToNumericFormat("-$0.006");             // returns number as : -0.006
const number16 =  convertToNumericFormat("$2,320.006");          // returns number as : 2320.006
const number17 =  convertToNumericFormat("$2,320.006,23");       // returns number as : 0
const number18 =  convertToNumericFormat("2,320.00623");         // returns number as : 0

console.log(number1);
console.log(number2);
console.log(number3);
console.log(number4);
console.log(number5);
console.log(number6);
console.log(number7);
console.log(number8);
console.log(number9);
console.log(number10);
console.log(number11);
console.log(number12);
console.log(number13);
console.log(number14);
console.log(number15);
console.log(number16);
console.log(number17);
console.log(number18);