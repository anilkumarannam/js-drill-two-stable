const fullName = require("../problem4.js");

const obj = {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"};
const obj1 = {"first_name": "aNiL", "middle_name": "KUMAR", "last_name": "annaM"};
const obj2 = {"first_name": "AmIT", "last_name": "TenDulKaR"};
const obj4 = {"last_name": "TenDulKaR"};
const obj5 = {"first_name": "GaTes"};
const obj6 = {"middle_name": "SaChin"};

const fullName1 = fullName(obj);         // returns  : John Doe Smith
const fullName2 = fullName(obj1);        // returns  : Anil Kumar Annam
const fullName3 = fullName(obj2);        // returns  : Amit Tendulkar
const fullName4 = fullName(null);        // returns  : ""
const fullName5 = fullName(undefined);   // returns  : ""
const fullName6 = fullName(obj4);        // returns  : ""
const fullName7 = fullName(obj5);        // returns  : ""
const fullName8 = fullName(obj6);        // returns  : ""

console.log(fullName1);
console.log(fullName2);
console.log(fullName3);
console.log(fullName4);
console.log(fullName5);
console.log(fullName6);
console.log(fullName7);
console.log(fullName8);