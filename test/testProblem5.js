const convertArrayToString = require("../problem5.js");

const array =["quick","brown","fox", "jumping"];

const convertedString = convertArrayToString(array);      // Returns as : quick brown fox jumping.
const convertedString2 = convertArrayToString([]);        // Returns as : ""
const convertedString1 = convertArrayToString(null);      // Returns as : ""
const convertedString3 = convertArrayToString(undefined); // Returns as : ""

console.log(convertedString);
console.log(convertedString1);
console.log(convertedString2);
console.log(convertedString3);