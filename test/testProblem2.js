const convertIpToNumberArray = require("../problem2.js");

const convertedIpArray1 = convertIpToNumberArray("221.255.123.123");  // Returns [221, 255, 123, 123]
const convertedIpArray2 = convertIpToNumberArray("221.255.123.1231"); // Returns []
const convertedIpArray3 = convertIpToNumberArray("221.255.123.123."); // Returns []
const convertedIpArray4 = convertIpToNumberArray("221.255.123.");     // Returns []
const convertedIpArray5 = convertIpToNumberArray("abcd");             // Returns []
const convertedIpArray6 = convertIpToNumberArray("");                 // Returns []
const convertedIpArray7 = convertIpToNumberArray(null);               // Returns []
const convertedIpArray8 = convertIpToNumberArray(undefined);          // Returns []

console.log(convertedIpArray8);