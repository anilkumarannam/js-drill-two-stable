const monthOfTheDate = require("../problem3.js");

const month1 = monthOfTheDate("12/31/2021");       // Returns December
const month2 = monthOfTheDate("12/1/2021");        // Returns December
const month3 = monthOfTheDate("1/1/2021");         // Returns January
const month4 = monthOfTheDate("2/29/2020");        // Returns February
const month5 = monthOfTheDate("2/292/2020");       // Returns Invalid date
const month6 = monthOfTheDate("abcd");             // Returns Invalid date
const month7 = monthOfTheDate("");                 // Returns Invalid date
const month8 = monthOfTheDate(null);               // Returns Invalid date
const month9 = monthOfTheDate(undefined);          // Returns Invalid date

console.log(month1);
console.log(month2);
console.log(month3);
console.log(month4);
console.log(month5);
console.log(month6);
console.log(month7);
console.log(month8);
console.log(month9);