
const convertToNumericFormat = inputString => {

    if(inputString!==null && inputString !== undefined){
        const regex = /^-?\$\d?(\d\,?)*\d+\.?\d+$/g;
        if(regex.test(inputString)){
            const numericString = inputString.replace(/[$,]/g,"");
            const rx = /^-?\d+\.\d+$/g
            return rx.test(numericString) ? parseFloat(numericString):parseInt(numericString);
        }
        return 0;
    }
}

module.exports = convertToNumericFormat;
