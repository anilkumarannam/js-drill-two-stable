const convertArrayToString = array => {
    if (array !== null && array !== undefined) {
        if (array.length < 1) {
            return "";
        }
        return array.join(" ");
    }
    return "";
}
module.exports = convertArrayToString;