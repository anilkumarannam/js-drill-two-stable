
const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

const monthOfTheDate = inputDate => {

    if (inputDate !== null && inputDate !== undefined) {
        const date = new Date(inputDate);
        if (JSON.stringify(date) !== "null") {
            const monthIndex = date.getMonth();
            return months[monthIndex];
        }
    }
    return "Invalid date"
}

module.exports = monthOfTheDate;